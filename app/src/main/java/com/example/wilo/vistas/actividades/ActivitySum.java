package com.example.wilo.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.wilo.R;

public class ActivitySum extends AppCompatActivity implements View.OnClickListener {
    EditText uno, dos, res;
    Button go;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sum);
        cargarComponents();
    }

    private void cargarComponents() {
        uno = findViewById(R.id.txtUno);
        dos = findViewById(R.id.txtDos);
        res = findViewById(R.id.textRequest);
        go = findViewById(R.id.btngo);
        go.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        int valor1 = Integer.parseInt(uno.getText().toString());
        int valor2 = Integer.parseInt(uno.getText().toString());
        int suma = valor1 + valor2;
        go.setText(suma);
    }
}
