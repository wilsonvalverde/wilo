package com.example.wilo.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.wilo.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ActividadSD extends AppCompatActivity implements View.OnClickListener {
    EditText cajaNombre, cajaApellidos;
    Button botonExcribir, botonLeer;
    TextView datos ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_sd);
        cargarComponentes();
    }
    public void cargarComponentes(){
        datos = findViewById(R.id.lblSD);
        cajaNombre = findViewById(R.id.txtNombreSD);
        cajaApellidos = findViewById(R.id.txtApellidoSD);
        botonExcribir = findViewById(R.id.btnEscribirSD);
        botonLeer = findViewById(R.id.btnLeerSD);

        botonExcribir.setOnClickListener(this);
        botonLeer.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnEscribirSD:
                try{


                File ruta = Environment.getExternalStorageDirectory();
                File file = new File(ruta.getAbsoluteFile(),"sd.txt");
                OutputStreamWriter escritor = new OutputStreamWriter(new FileOutputStream(file));
                escritor.write(cajaNombre.getText().toString() +
                        ","+ cajaApellidos.getText().toString()+";");
                escritor.close();
                }catch (Exception ex){
                    Log.e("Error SD al escribir", ex.getMessage());

                }
                break;

            case R.id.btnLeerSD:
                try{
                    File ruta = Environment.getExternalStorageDirectory();
                    File file = new File(ruta.getAbsoluteFile(),"sd.txt");
                    BufferedReader lector =new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                    datos.setText(lector.readLine());


                } catch (IOException e) {
                    Log.e("Error SD al leer", e.getMessage());
                }
        }
    }
}
