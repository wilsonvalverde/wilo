package com.example.wilo.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.wilo.R;
import com.example.wilo.models.Artista;
import com.example.wilo.vistas.adapter.ArtistaAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ActividadMemoriaPrograma extends AppCompatActivity implements View.OnClickListener {
    RecyclerView reviewListado;
    ArtistaAdapter adapter;
    TextView ver;
    Button mostrar;

    List<Artista> artistasList;
    List<Artista> hola;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_memoria_programa);
        tomarControl();
    }
    public void tomarControl(){
        reviewListado = findViewById(R.id.reviewmemoria);
        mostrar = findViewById(R.id.btnreviewmemorias);
        ver = findViewById(R.id.lblreviewmemory);
        mostrar.setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {
            switch (v.getId()){
                case R.id.btnreviewmemorias:
                    try{
                        InputStream input = getResources().openRawResource(R.raw.archivo_raw);
                        BufferedReader vector = new BufferedReader(new InputStreamReader(input));
                        String cadena = vector.readLine();
                        ver.setText(cadena);

                    artistasList = new ArrayList<Artista>();
                    Artista a = new Artista();
                    String[] listaA = cadena.split(";");

                    for (int i=0; i<2; i++){
                        String[] listaA1 = cadena.split(",");
                        a.setNombres(listaA1[0]);
                        a.setNombres(listaA1[1]);
                        artistasList.add(a);
                    }
                    adapter = new ArtistaAdapter(artistasList);
                    reviewListado.setLayoutManager(new LinearLayoutManager(this));
                    reviewListado.setAdapter(adapter);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }



                    break;



    }

    }
}
