package com.example.wilo.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wilo.R;
import com.example.wilo.models.Carro;

public class ActividadCarroOrm extends AppCompatActivity implements View.OnClickListener {
    Button add,all,search,deleteonly;
    RecyclerView orm;
    EditText marca,modelo,anio,placa;
    TextView datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_carro_orm);
        tomarControl();
    }
    public void tomarControl(){
        add = findViewById(R.id.btnOrmAdd);
        all = findViewById(R.id.btnOrmGetAll);
        search = findViewById(R.id.btnOrmGetSingle);
        orm = findViewById(R.id.viewORM);
        datos = findViewById(R.id.viewOrmDatos);
        deleteonly = findViewById(R.id.btnOrmDeleteSingle);

        marca = findViewById(R.id.txtOrmMarca);
        modelo = findViewById(R.id.txtOrmModelo);
        anio = findViewById(R.id.txtOrmAnio);
        placa = findViewById(R.id.txtOrmPlaca);

        add.setOnClickListener(this);
        all.setOnClickListener(this);
        search.setOnClickListener(this);
        deleteonly.setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnOrmAdd:
                Carro carro = new Carro();
                carro.setPlaca(placa.getText().toString());
                carro.setAnio(Integer.parseInt(anio.getText().toString()));
                carro.setMarca(marca.getText().toString());
                carro.setModelo(modelo.getText().toString());
                carro.save();
                Toast.makeText(this,"Guardado",Toast. LENGTH_SHORT).show();
                break;
            case R.id.btnOrmGetAll:

                datos.setText("Numero de carros " + Carro.getALLCarro());

                Toast.makeText(this,"Presentado",Toast. LENGTH_SHORT).show();

                break;
            case R.id.btnOrmGetSingle:

                datos.setText("Datos por placa  "+Carro.getOnlyPlacaCarro(placa.getText().toString()));
            case R.id.btnOrmDeleteSingle:
                Carro carrodeleteonly = new Carro();
                carrodeleteonly = Carro.getOnlyPlacaCarro(placa.getText().toString());
                carrodeleteonly.delete();

                datos.setText("Datos por placa  "+Carro.getOnlyPlacaCarro(placa.getText().toString()));



        }

    }
}
