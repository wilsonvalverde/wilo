package com.example.wilo.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.wilo.R;
import com.example.wilo.models.Artista;
import com.example.wilo.vistas.adapter.ArtistaAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class ActividadMI extends AppCompatActivity implements View.OnClickListener{
    Button botonGuardar, botonBuscarTodos;
    EditText txtnombre,txtapellido;
    TextView datos;
    RecyclerView reviewListado;
    ArtistaAdapter adapter;
    List<Artista> artistasList;
    List<Artista> hola;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_mi);
        tomarControl();
    }
    private void tomarControl(){
        artistasList = new ArrayList<Artista>();
        hola = new ArrayList<>();
        botonGuardar = findViewById(R.id.btnAdd);
        botonBuscarTodos = findViewById(R.id.btnsearch);
        txtnombre = findViewById(R.id.txtnombre);
        txtapellido = findViewById(R.id.txtapellido);

        datos = findViewById(R.id.datos);

        reviewListado = findViewById(R.id.viewListado);


        botonGuardar.setOnClickListener(this);
        botonBuscarTodos.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnAdd:
                try{
                    OutputStreamWriter escritor =
                            new OutputStreamWriter
                                    (openFileOutput("archivo.txt", Context.MODE_APPEND));
                    escritor.write(txtnombre.getText().toString()+","+txtapellido.getText().toString()+";");
                    escritor.close();
                    Artista artista = new Artista();
                    artista.setNombres(txtnombre.getText().toString());
                    artista.setApellidos(txtapellido.getText().toString());
                    artistasList.add(artista);





                }catch (Exception ex){
                    Log.e("ArchivoMi","error de escritura"+ex.getMessage());
                }

                break;
            case R.id.btnsearch:

               try {

                   BufferedReader lector = new BufferedReader(new InputStreamReader(openFileInput("archivo.txt")));
                   String lineas  =lector.readLine();
                   datos.setText(lineas);

                   adapter = new ArtistaAdapter(artistasList);
                   reviewListado.setLayoutManager(new LinearLayoutManager(this));
                   reviewListado.setAdapter(adapter);

                   lector.close();


               }catch (Exception ex){
                   Log.e("ArchivoMi","error de escritura"+ex.getMessage());
               }


        }

    }
}
