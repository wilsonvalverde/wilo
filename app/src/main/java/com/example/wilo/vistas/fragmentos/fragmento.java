package com.example.wilo.vistas.fragmentos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.example.wilo.R;
import com.example.wilo.models.Comunicador;

public class fragmento extends AppCompatActivity implements Comunicador, View.OnClickListener ,Uno.OnFragmentInteractionListener,Dos.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragmento2);
        cargarContenedores();
    }
    private void cargarContenedores(){
        Uno frag = new Uno();
        FragmentTransaction tran1 = getSupportFragmentManager().beginTransaction();
        tran1.replace(R.id.contenedor1,frag);
        tran1.commit();
        Dos frag2 = new Dos();
        FragmentTransaction tran2 = getSupportFragmentManager().beginTransaction();
        tran2.replace(R.id.contenedor2, frag2);
        tran2.commit();

    }
    @Override
    public void onClick(View v) {

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void responder(String datos) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Dos frgr1 = (Dos) fragmentManager.findFragmentById(R.id.fragment2);
        frgr1.cambiarTexto(datos);


    }
}
