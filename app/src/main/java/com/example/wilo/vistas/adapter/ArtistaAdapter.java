package com.example.wilo.vistas.adapter;

import android.app.Dialog;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wilo.R;
import com.example.wilo.models.Artista;

import java.util.List;

public class ArtistaAdapter extends RecyclerView.Adapter<ArtistaAdapter.ViewHolderArtista> {
    List<Artista> lista;
    public ArtistaAdapter(List<Artista> lista){
        this.lista = lista;
    }
    @NonNull
    @Override
    public ViewHolderArtista onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_artista,null );

        return new ViewHolderArtista(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onBindViewHolder(@NonNull ViewHolderArtista holder, int position) {
        holder.datoNombres.setText(lista.get(position).getNombres());
        holder.datoApellidos.setText(lista.get(position).getApellidos());
        

    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public static class ViewHolderArtista extends RecyclerView.ViewHolder{
        TextView datoNombres;
        TextView datoApellidos;
        ImageView foto;

        public ViewHolderArtista(View itemView){
            super(itemView);
            datoNombres = itemView.findViewById(R.id.lblNombresArtitas);
            datoApellidos = itemView.findViewById(R.id.lblApellidoArtista);


        }
    }
}
