package com.example.wilo.vistas.actividades;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.wilo.R;
import com.example.wilo.models.Godos;
import com.example.wilo.vistas.adapter.GodoAdapter;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class ActividadLeerXml extends AppCompatActivity implements View.OnClickListener {
    Button boton_mostrar;
    TextView mostrar;
    RecyclerView recicler_mostrar_P;

    GodoAdapter adapter;
    List<Godos> listarArtistas;
    List<Godos> lista_final;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_leer_xml);
        tomar_control();
    }
    public void tomar_control(){
        recicler_mostrar_P = findViewById(R.id.recycler_lista);
        boton_mostrar=findViewById(R.id.btn_xml);
        mostrar = findViewById(R.id.lbl_mostrar);
        boton_mostrar.setOnClickListener(this);

    }
    public  void onbtener(){
        try{
            InputStream input = getResources().openRawResource(R.raw.reyes);
            BufferedReader lect = new BufferedReader(new InputStreamReader(input));
            String datos = lect.readLine();
            Log.e("p",datos);


        }catch (Exception ex){

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_xml:
                try{
                    lista_final = new ArrayList<Godos>();
                   // Godos g2 = new Godos();
                    //g2.setNombre("sasa");
                    //g2.setPeriodo("sa");
                    //lista_final.add(g2);
                  DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                   Document doc = builder.parse(getResources().openRawResource(R.raw.reyes),null);
                    NodeList godos = doc.getElementsByTagName("godo");
                    Toast.makeText(this,"numero de reyes es "+godos.getLength(),Toast.LENGTH_SHORT).show();
                    mostrar.setText("numero "+ godos.getLength());
                   for (int i=0; i < godos.getLength();i++){
                       Godos g = new Godos();
                        g.setNombre(godos.item(i).getTextContent());
                        //g.setPeriodo(godos.item(i).getTextContent());
                        lista_final.add(g);


                    }

                    adapter = new GodoAdapter(lista_final);
                    recicler_mostrar_P.setLayoutManager(new LinearLayoutManager(this));
                    recicler_mostrar_P.setAdapter(adapter);


                }catch (Exception ex){

                }

                break;


        }
    }
    }

