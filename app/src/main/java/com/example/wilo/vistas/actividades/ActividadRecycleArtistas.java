package com.example.wilo.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.wilo.R;
import com.example.wilo.models.Artista;
import com.example.wilo.vistas.adapter.ArtistaAdapter;

import java.util.ArrayList;
import java.util.List;
public class ActividadRecycleArtistas extends AppCompatActivity {
    RecyclerView recyclerViewAtistas;
    ArtistaAdapter adapter;
    List<Artista> artistasList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_recycle_artistas);
        tomarControl();
        cargarRecycle();
    }
    private void  tomarControl(){
        recyclerViewAtistas = findViewById(R.id.recyclerArtistas);


    }
    private void cargarRecycle(){
        artistasList = new ArrayList<Artista>();
        Artista artista1 = new Artista();
        artista1.setNombres("Luis");
        artista1.setApellidos("Miguel");
        Artista artista2 = new Artista();
        artista2.setNombres("Don");
        artista2.setApellidos("Medardo");
        artista2.setFoto(R.drawable.tren);
        Artista artista3 = new Artista();
        artista3.setNombres("wilson");
        artista3.setApellidos("Valverde");
        artista3.setFoto(R.drawable.image);


        artistasList.add(artista1);
        artistasList.add(artista2);
        artistasList.add(artista3);

        adapter = new ArtistaAdapter(artistasList);
        recyclerViewAtistas.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewAtistas.setAdapter(adapter);
    }
}
