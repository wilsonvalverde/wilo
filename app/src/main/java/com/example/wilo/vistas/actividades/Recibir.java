package com.example.wilo.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.wilo.R;

public class Recibir extends AppCompatActivity implements View.OnClickListener{
    TextView nombre,apellido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir);
        cargarComponentes();
        cargar();
    }
    private void cargarComponentes(){

        nombre = findViewById(R.id.textView1);
        apellido = findViewById(R.id.textView2);

    }
    private void cargar(){
        Bundle bundle = this.getIntent().getExtras();
        nombre.setText(bundle.getString("nombres"));
        apellido.setText(bundle.getString("apellidos"));
        //String nombre1 = getIntent().getStringExtra("nombre");
        //String apellido1 = getIntent().getStringExtra("apellido");
       // nombre.setText(nombre1);
        //apellido.setText(apellido1);
    }


    @Override
    public void onClick(View v) {


    }
}
