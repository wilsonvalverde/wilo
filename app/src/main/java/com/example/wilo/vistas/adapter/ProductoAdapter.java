package com.example.wilo.vistas.adapter;

import  android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wilo.R;
import com.example.wilo.models.Artista;
import com.example.wilo.models.Producto;

import java.util.List;

public class ProductoAdapter extends RecyclerView.Adapter<ProductoAdapter.ViewHolderProducto> {
    List<Producto> lista;
    public ProductoAdapter(List<Producto> lista){
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolderProducto onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_producto,null );

        return new ProductoAdapter.ViewHolderProducto(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderProducto holder, int position) {
        holder.datoCodigo.setText(""+lista.get(position).getCodigo());
        holder.datoDescripcion.setText(lista.get(position).getDescripcion());
        holder.datoPrecio.setText(""+ lista.get(position).getPrecio());
        holder.datoCantidad.setText(""+lista.get(position).getCantidad());

    }

    @Override
    public int getItemCount() {
        return 0;
    }



    public static class ViewHolderProducto extends RecyclerView.ViewHolder {
        TextView datoCodigo;
        TextView datoDescripcion;
        TextView datoPrecio;
        TextView datoCantidad;
        public ViewHolderProducto(@NonNull View itemView) {
            super(itemView);
            datoCodigo = itemView.findViewById(R.id.lblCodigoProducto);
            datoDescripcion = itemView.findViewById(R.id.lblDescProducto);
            datoPrecio = itemView.findViewById(R.id.lblPrecioProducto);
            datoCantidad = itemView.findViewById(R.id.lblCantidadProducto);

        }
    }
}
