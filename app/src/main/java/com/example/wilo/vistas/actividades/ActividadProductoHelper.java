package com.example.wilo.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.wilo.R;
import com.example.wilo.controller.ProductoController;
import com.example.wilo.models.Producto;
import com.example.wilo.vistas.adapter.ProductoAdapter;

import java.util.ArrayList;
import java.util.List;

public class ActividadProductoHelper extends AppCompatActivity implements View.OnClickListener {
    EditText txtcodigo,txtdescripcion,txtprecio,txtcantidad;
    Button btnadd,btnall,btnupdate,btndelete;
    RecyclerView reciclerProducto;
    List<Producto> productoList;
    ProductoController helper;
    ProductoAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_producto_helper);
        tomarControl();
    }
    private void tomarControl(){
        txtcodigo = findViewById(R.id.textProductoCodigo);
        txtcantidad = findViewById(R.id.textProductoCantidad);
        txtprecio = findViewById(R.id.textProductoPrecio);
        txtdescripcion = findViewById(R.id.textProductoDescripcion);
        reciclerProducto = findViewById(R.id.RecyclerViewProducto);
        btnadd = findViewById(R.id.btn_add);
        btnall = findViewById(R.id.btnProductoGetall);
        btnupdate = findViewById(R.id.btnProductoUpdate);
        btndelete = findViewById(R.id.btnProductoDelete);
        productoList = new ArrayList<Producto>();

        btnadd.setOnClickListener(this);
        btnall.setOnClickListener(this);
        btnupdate.setOnClickListener(this);
        btndelete.setOnClickListener(this);
        helper = new ProductoController(this,"name",null,1);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btn_add:

                Producto prod = new Producto();
                int codigo = Integer.parseInt(txtcodigo.getText().toString());
                prod.setDescripcion(txtdescripcion.getText().toString());
                int cantidad = Integer.parseInt(txtcantidad.getText().toString());
                double precio = Double.parseDouble(txtprecio.getText().toString());
                prod.setCodigo(codigo);
                prod.setCantidad(cantidad);
                prod.setPrecio(precio);
                helper.insertar(prod);
                Toast.makeText(this,"Guardado",Toast. LENGTH_SHORT).show();

                break;
            case R.id.btnProductoGetall:
                adapter = new ProductoAdapter(productoList);
                reciclerProducto.setLayoutManager(new LinearLayoutManager(this));
                reciclerProducto.setAdapter(adapter);
                Toast.makeText(this,"listar",Toast. LENGTH_SHORT).show();
                break;
            case R.id.btnProductoUpdate:
                helper = new ProductoController(this,"name",null,1);
                SQLiteDatabase sqlM = helper.getWritableDatabase();
                Producto produ = new Producto();
                int codigou = Integer.parseInt(txtcodigo.getText().toString());
                produ.setDescripcion(txtdescripcion.getText().toString());
                int cantidadu = Integer.parseInt(txtcantidad.getText().toString());
                double preciou = Double.parseDouble(txtprecio.getText().toString());
                produ.setCodigo(codigou);
                produ.setCantidad(cantidadu);
                produ.setPrecio(preciou);
                helper.modificar(produ);
                sqlM.close();
            case R.id.btnProductoDelete:
                helper = new ProductoController(this,"name",null,1);
                SQLiteDatabase sqlD = helper.getReadableDatabase();
                Producto prodd = new Producto();
                int codigod = Integer.parseInt(txtcodigo.getText().toString());
                prodd.setCodigo(codigod);
                helper.eliminar(prodd);
                sqlD.close();
        }


    }
}
