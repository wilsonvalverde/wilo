package com.example.wilo.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.wilo.R;
import com.example.wilo.vistas.fragmentos.FrgDos;
import com.example.wilo.vistas.fragmentos.FrgUno;

public class ActivityFragmento extends AppCompatActivity implements View.OnClickListener, FrgUno.OnFragmentInteractionListener, FrgDos.OnFragmentInteractionListener {
    Button botonFgr1,botonFgr2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragmento);
        cargarComponentes();
    }
    private void cargarComponentes(){
        botonFgr1 = findViewById(R.id.btnFRG1);
        botonFgr2 = findViewById(R.id.btnFrg2);
        botonFgr1.setOnClickListener(this);
        botonFgr2.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnFRG1:
                FrgUno fragmento = new FrgUno();
                FragmentTransaction transaccion1 = getSupportFragmentManager().beginTransaction();
                transaccion1.replace(R.id.contenedorFragmentos,fragmento);
                transaccion1.commit();
                break;
            case R.id.btnFrg2:
                FrgDos fragmento2 = new FrgDos();
                FragmentTransaction transaction2 = getSupportFragmentManager().beginTransaction();
                transaction2.replace(R.id.contenedorFragmentos, fragmento2);
                transaction2.commit();
                break;
        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
