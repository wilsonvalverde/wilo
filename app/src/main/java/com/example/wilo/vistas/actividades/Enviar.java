package com.example.wilo.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.wilo.R;

public class Enviar extends AppCompatActivity implements View.OnClickListener {

    EditText nombre,apellido;
    Button enviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviar);
        cargarComponentes();
    }
    private void cargarComponentes(){
        nombre = findViewById(R.id.txtNombreEnviarParametro);
        apellido = findViewById(R.id.txtApellidoParametro);
        enviar = findViewById(R.id.btnEnviarParametro);
        enviar.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        Intent intent = new Intent(Enviar.this, Recibir.class);
       // intent.putExtra("nombre", nombre.getText()+"");
       // intent.putExtra("apellido", apellido.getText()+"");
        bundle.putString("nombres", nombre.getText().toString());
        bundle.putString("apellidos", apellido.getText().toString());
        intent.putExtras(bundle);
        startActivity(intent);

    }
}
