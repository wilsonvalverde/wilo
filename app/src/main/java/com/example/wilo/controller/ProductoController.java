package com.example.wilo.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.wilo.models.Producto;

import java.util.ArrayList;
import java.util.List;

public class ProductoController extends SQLiteOpenHelper {
    public ProductoController(Context context,String name,
                              SQLiteDatabase.CursorFactory factory,
                              int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table producto(id integer primary key autoincrement," +
                " codigo integer not null," +
                " descripcion text not null," +
                " precio real not null," +
                " cantidad integer not null)");

    }
    public void insertar(Producto producto){
        ContentValues valores = new ContentValues();
        valores.put("codigo",producto.getCodigo());
        valores.put("descripcion",producto.getDescripcion());
        valores.put("precio",producto.getPrecio());
        valores.put("cantidad",producto.getCantidad());
        this.getWritableDatabase().insert(
                "producto",null,valores);
    }
    public List<Producto> list(){
        List<Producto> lista = new ArrayList<Producto>();
        Cursor cursor = this.getReadableDatabase().rawQuery("select* from producto",null);
        if (cursor.moveToFirst())
            do {
                Producto p = new Producto();
                p.setCodigo(cursor.getInt(cursor.getColumnIndex("codigo")));
                p.setDescripcion(cursor.getString(cursor.getColumnIndex("descripcion")));
                p.setPrecio(cursor.getDouble(cursor.getColumnIndex("precio")));
                p.setCantidad(cursor.getInt(cursor.getColumnIndex("cantidad")));
                lista.add(p);
            }while(cursor.moveToNext());
            cursor.close();
           return lista;
    }
    public void modificar(Producto producto){
        ContentValues valores = new ContentValues();
        valores.put("codigo",producto.getCodigo());
        valores.put("descripcion",producto.getDescripcion());
        valores.put("precio",producto.getPrecio());
        valores.put("cantidad",producto.getCantidad());
        this.getWritableDatabase().update(
          "producto",valores,"codigo = "+producto.getCodigo(),null);
    }
     public void eliminar(Producto producto){
        this.getWritableDatabase().delete(
          "producto","codigo="+producto.getCodigo(),null);
     }
     public List<Producto> getProductosByCode(String code) {
         List<Producto> lista = new ArrayList<Producto>();
         Cursor cursor = this.getReadableDatabase().rawQuery(
                 "select* from producto where codigo =" + code, null
         );
         if (cursor.moveToFirst())
             do {
                 Producto p = new Producto();
                 p.setCodigo(cursor.getInt(cursor.getColumnIndex("Codigo")));
                 p.setDescripcion(cursor.getString(cursor.getColumnIndex("Descripcion")));
                 p.setPrecio(cursor.getDouble(cursor.getColumnIndex("Precio")));
                 p.setCantidad(cursor.getInt(cursor.getColumnIndex("Cantidad")));
                 lista.add(p);
             } while (cursor.moveToNext());
         cursor.close();
         return lista;
     }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
