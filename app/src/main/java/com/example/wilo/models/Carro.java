package com.example.wilo.models;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name="carro")
public class Carro extends Model {
    @Column(name = "placa", unique = true)
    private String placa;
    @Column(name = "modelo", notNull = true)
    private String modelo;
    @Column(name = "marca", notNull = true)
    private String marca;
    @Column(name = "anio", notNull = true)
    private int anio;

    public Carro(){
        super();
    }

    public Carro(String placa,String modelo,String marca,int anio){
        this.placa = placa;
        this.modelo = modelo;
        this.marca = marca;
        this.anio = anio;
    }
    public static List<Carro> getALLCarro(){
        return new Select().from(Carro.class).execute();
    }
    public static Carro getOnlyPlacaCarro(String placa){
        return new Select().from(Carro.class).where("placa =?",placa).executeSingle();
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }
}
