package com.example.wilo.models;

public interface Comunicador {
    public void responder(String datos);
}
