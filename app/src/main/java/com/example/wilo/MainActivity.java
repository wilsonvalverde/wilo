package com.example.wilo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.wilo.models.Artista;
import com.example.wilo.models.config.AplicacionORM;
import com.example.wilo.vistas.actividades.ActividadCarroOrm;
import com.example.wilo.vistas.actividades.ActividadLeerXml;
import com.example.wilo.vistas.actividades.ActividadMI;
import com.example.wilo.vistas.actividades.ActividadMemoriaPrograma;
import com.example.wilo.vistas.actividades.ActividadProductoHelper;
import com.example.wilo.vistas.actividades.ActividadRecycleArtistas;
import com.example.wilo.vistas.actividades.ActividadSD;
import com.example.wilo.vistas.actividades.ActivityFragmento;
import com.example.wilo.vistas.actividades.ActivityLogin;
import com.example.wilo.vistas.actividades.ActivitySum;
import com.example.wilo.vistas.actividades.Enviar;
import com.example.wilo.vistas.fragmentos.fragmento;

public class MainActivity extends AppCompatActivity implements OnClickListener{
    Button botonLogin,
            botonSuma,
            botonParametro,
            botonActividad,
            botonArtista,
            botonMI,
            botonmemoria,
            botonxml,
            botonSD,
            botonHelper,
            botonProducto,
            botonOrm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cargarComponentes();


    }
    private void cargarComponentes() {
        botonLogin = findViewById(R.id.btnLOGIN);
        botonSuma = findViewById(R.id.btnSUMAR);
        botonParametro = findViewById(R.id.btnEnviarParametro);
        botonActividad = findViewById(R.id.btnActividad);
        botonArtista = findViewById(R.id.btnArtista);
        botonMI = findViewById(R.id.btnMI);
        botonmemoria = findViewById(R.id.btnmemoria);
        botonxml = findViewById(R.id.btnXML);
        botonSD = findViewById(R.id.btnSD);
        botonHelper = findViewById(R.id.btnHelper);
        botonProducto = findViewById(R.id.btnProductos);
        botonOrm = findViewById(R.id.btnOrm);

        ////////////////////////////////////////////////////////////////////////
        botonLogin.setOnClickListener(this);
        botonSuma.setOnClickListener(this);
        botonParametro.setOnClickListener(this);
        botonActividad.setOnClickListener(this);
        botonArtista.setOnClickListener(this);
        botonMI.setOnClickListener(this);
        botonmemoria.setOnClickListener(this);
        botonxml.setOnClickListener(this);
        botonSD.setOnClickListener(this);
        botonHelper.setOnClickListener(this);
        botonProducto.setOnClickListener(this);
        botonOrm.setOnClickListener(this);

    }
    @Override
    public void onClick(View view){
        Intent intent = null;
        switch (view.getId()){
            case R.id.btnLOGIN:
                intent = new Intent(MainActivity.this, ActivityLogin.class);
                startActivity(intent);
                break;
            case R.id.btnSUMAR:
                intent = new Intent(MainActivity.this, ActivitySum.class);
                startActivity(intent);
                break;
            case R.id.btnEnviarParametro:
                intent = new Intent(MainActivity.this, Enviar.class);
                startActivity(intent);
                break;
            case R.id.btnActividad:
                intent = new Intent(MainActivity.this, fragmento.class);
                startActivity(intent);
                break;
            case R.id.btnArtista:
                intent = new Intent(MainActivity.this, ActividadRecycleArtistas.class);
                startActivity(intent);
                break;
            case R.id.btnMI:
                intent = new Intent(MainActivity.this, ActividadMI.class);
                startActivity(intent);
                break;
            case R.id.btnmemoria:
                intent = new Intent(MainActivity.this, ActividadMemoriaPrograma.class);
                startActivity(intent);
                break;
            case R.id.btnXML:
                intent = new Intent(MainActivity.this, ActividadLeerXml.class);
                startActivity(intent);
                break;
            case R.id.btnSD:
                intent = new Intent(MainActivity.this, ActividadSD.class);
                startActivity(intent);
                break;
            case R.id.btnHelper:
                intent = new Intent(MainActivity.this, ActividadProductoHelper.class);
                startActivity(intent);
                break;
            case R.id.btnProductos:
                intent = new Intent(MainActivity.this,ActividadProductoHelper.class);
                startActivity(intent);
                break;
            case R.id.btnOrm:
                intent = new Intent(MainActivity.this, ActividadCarroOrm.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //METHOD FOR CREATE MENU
        //MenuInflater permite create only object for drove archive xml
        // method inflate permite add menu implement only archive xml in activity
        MenuInflater inflaterMenu = getMenuInflater();
        inflaterMenu.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
       // This method permite do event only item menu son
        Intent intent;
        switch (item.getItemId()){
            case R.id.opcionLogin:
                intent = new Intent(MainActivity.this, ActivityLogin.class);
                startActivity(intent);
                break;
            case R.id.opcionSumar:
                intent = new Intent(MainActivity.this, ActivitySum.class);
                startActivity(intent);
                break;
            case R.id.opcionParametro:
                intent = new Intent(MainActivity.this, Enviar.class);
                startActivity(intent);
                break;
            case R.id.opcionColores:
                intent = new Intent(MainActivity.this, ActivityFragmento.class);
                startActivity(intent);
                break;
            case R.id.opcionDlgSumar:
                final Dialog dlgSumar = new Dialog(MainActivity.this);
                dlgSumar.setContentView(R.layout.dlg_sumar);
                final EditText cajaN1 = dlgSumar.findViewById(R.id.txtN1Dgl);
                final EditText cajaN2 = dlgSumar.findViewById(R.id.txtN2Dlg);
                Button btnSumarDlg = dlgSumar.findViewById(R.id.btnSumarDlg);
                btnSumarDlg.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resultado = Integer.parseInt(cajaN1.getText().toString()) +
                                Integer.parseInt(cajaN2.getText().toString());
                        Toast.makeText(MainActivity.this,"suma es" + resultado, Toast.LENGTH_SHORT).show();
                        dlgSumar.hide();
                    }
                });
                dlgSumar.show();
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
